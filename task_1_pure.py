from math import log
import pickle
import pandas as pd


def csv_to_df(csvName):
    '''
    Read data from csv file into a pandas dataframe
    '''
    print("[log] Reading data from {} into a pandas dataframe ...".format(csvName))
    wiki_df = pd.read_csv(csvName, sep='\t')
    print("[log] Done reading data!")
    return wiki_df


def get_reverts(dataFrame):
    '''
    Get a list of Reverts: Changes to a previous version of the article
     - This will be noted by a value of 1 in the revert column
    '''
    print("[log] Getting a list of all reverts ...")
    reverter_df = dataFrame.loc[dataFrame['revert'] == 1]
    num_reverts = reverter_df.shape[0]
    print("[log] found {} reverts !".format(num_reverts))
    return reverter_df, num_reverts


def get_original_version(wiki_df, revert):
    '''
    Get original version of the aricle by getting the edit which has
    - same article name
    - same version number
    - revert flag = 0
    '''
    reverter_version, reverter_title = revert[4], revert[1]
    title_match = wiki_df['title'] == reverter_title
    version_match = wiki_df['version'] == reverter_version
    norevert_match = wiki_df['revert'] == 0
    original_version = wiki_df[title_match & version_match & norevert_match]
    return original_version


def get_status(dataFrame, name, time):
    '''
    Status is the log of the numer of edits made by a user before a certain time
    '''

    # Step 1 : get all articles edited by that user before the given time
    name_match = dataFrame['user'] == name
    previous_match = dataFrame['time'] < time
    reverter_previous_edits = dataFrame[name_match & previous_match]

    # Step 2 : get the number of the articles
    number_edits = reverter_previous_edits.shape[0]

    # Step 3 : get the log of that number, or return 0 if no articles are found to avoid the error of log(0)
    status = log(number_edits) if number_edits > 0 else 0
    return status


def main():
    '''
    Read data from csv file into a pandas dataframe
    '''
    csvName = "enwiki_2002.txt"
    wiki_df = csv_to_df(csvName)

    '''
    Get a list of Reverts: Changes to a previous version of the article
    '''
    reverter_df, num_reverts = get_reverts(wiki_df)

    '''
    if no reverts are found, exit
    '''
    if reverter_df.empty:
        print("[log] No reverts found! Exiting!")
        quit()

    '''
    Get the details of each revert
    '''
    print("[log] Getting info on each revert ...")

    result_task1 = []
    progress = 0
    for revert in reverter_df.itertuples():
        '''
        print the output every 100 reverts
        '''
        progress += 1
        if progress % 100 == 0:
            print("[log] Progress:", progress, "\\", num_reverts)

        '''
        Getting the event time:
        This will be the time when the reverter made the change.
        '''
        revert_time = revert[2]

        '''
        Getting Reverter Name
        '''
        reverter_name = revert[5]

        '''
        Get original version of the aricle
        '''
        original_version = get_original_version(wiki_df, revert)

        '''
        The edit that was reverted is the one immediatlay after that
        '''
        reverted = wiki_df.iloc[original_version.index[0] - 1]

        '''
        If the reverted has the same version as the restored, ignore it
        '''
        if revert[4] == reverted['version']:    # revert[4] is reverter version
            continue

        '''
        Ignoring self reverts
        '''
        reverted_name = reverted['user']
        if reverter_name == reverted_name:
            continue

        '''
        Getting status of reverter: logarithm of number of articles he edited before the revert
        '''
        reverter_status = get_status(wiki_df, reverter_name, revert_time)

        '''
        Getting status of reverted: logarithm of number of articles he edited before the revert
        '''
        reverted_status = get_status(wiki_df, reverted_name, revert_time)

        '''
        Store the revert in the specified format
        '''
        data = {
            'reverter_name': reverter_name,
            'reverted_name': reverted_name,
            'revert_time': revert_time,
            'reverter_status': reverter_status,
            'reverted_status': reverted_status
        }
        result_task1.append(data)

    '''
    Store the list of dicts to a pickle file
    '''
    pickleFileName = 'result_task1.pickle'
    print("[log] Storing {} reverts to {} ...".format(len(result_task1), pickleFileName))
    with open(pickleFileName, 'wb') as pickle_out:
        pickle.dump(result_task1, pickle_out)
    print("[log] Data storage complete! Task 1 is done :D")


if __name__ == "__main__":
    main()
