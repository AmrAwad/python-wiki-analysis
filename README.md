# Python Wiki Analysis

## Overview

This project intends to answer a computational social science question by analyzing large amounts of wiki dumps:

> Social comparison theory states that people strive to gain accurate self-evaluations and as a result, they tend to compare themselves to those who are similar. But since focus on relative performance heightens feelings of competitiveness, rivalry is stronger among similar individuals. This leads us to expect that the editors involved in the AB–BA motif tend to be closer in status than expected (that is, the status difference is smaller than for the other kinds of reverts).

> To find evidence that retaliation is more likely among status-equals, we need to compare the status difference between ediotrs involved in AB–BA motifs with the status difference between editors involved in any other type of revert.

* AB-BA motifs mean reverts where A reverts B  then B reverts A.

## Breakdown

The project is broken down into 3 tasks:

### Task 1: Get a list of reverts from wiki dumps
Storing the time, reverter & reverted names and status to a pickle file

### Task 2: Identify AB-BA motifs.
Saving AB & BA reverts as a single element in a pickle file.

### Task 3: Visualize the status differences
Using a histogram to visualize the status differences to answer the original question.

Note: the results were validated with a [T-Test](http://www.statisticshowto.com/probability-and-statistics/t-test/) with a 1000 runs using the following code snippet:

```
from scipy.stats import ttest_ind
import random
all_status_diff = [diff for diff in all_status_diff if diff not in abba_status_diff]
p_list = []
for _ in range(1000):
    all_status_diff = [random.choice(all_status_diff) for _ in range(len(abba_status_diff))]
    t, p = ttest_ind(abba_status_diff, all_status_diff, equal_var=False)
    p_list.append(p)
print("Average P = {} ".format(sum(p_list)/len(p_list)))

# Output: Average P = 1.6109243790372957e-10
```
