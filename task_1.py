import sqlite3
import pickle
from math import log

'''
Read data from file.
'''
fileName = "enwiki_2002.txt"
print("[log] Reading edits from {} ...".format(fileName))

with open(fileName, 'r', encoding="utf-8") as fh:
    edits = fh.readlines()[1:]  # Skip the file headers

print("[log] Done reading edits!")

'''
Create a database to store the data in
'''
dbName = "wiki.db"
print("[log] Creating edits table in database {} ...".format(dbName))

# The last line creates a constraint to avoid duplicate rows
create_table = """
CREATE TABLE IF NOT EXISTS edits
    (id INTEGER PRIMARY KEY,
    title TEXT, time TEXT,
    revert INT, version INT, user TEXT,
    CONSTRAINT unique_row UNIQUE (title, time, revert, version, user))
"""
conn = sqlite3.connect(dbName)
cur = conn.cursor()
cur.execute(create_table)
conn.commit()
conn.close()

print("[log] Table creaion completed!".format(dbName))

'''
Populate the database with the file contents
'''
print("[log] Populating the database {} ...".format(dbName))

insert_row = "INSERT OR IGNORE INTO edits(title, time, revert, version, user) VALUES(?, ?, ?, ?, ?)"
conn = sqlite3.connect(dbName)
cur = conn.cursor()
for edit in edits:
    row = edit.strip().split("\t")
    title = row[0].strip()
    # time = row[1].replace(":","").replace(" ","").replace("-")  # Store as YYYYMMDDHHMMSS for fast comparsion
    values = (title, row[1], row[2], row[3], row[4])    # (title, time, revert, version, user)
    cur.execute(insert_row, values)
conn.commit()
conn.close()

print("[log] Database Population complete!")

'''
Get a list of Reverters: The person who restored an earlier version of the article
 - This will be noted by a value of 1 in the revert coloumn
'''
print("[log] Getting a list of all reverts ...")

query_reverter = "SELECT * from edits WHERE revert=1"
conn = sqlite3.connect(dbName)
cur = conn.cursor()
cur.execute(query_reverter)
reverter_list = cur.fetchall()  # This returns a list of tuples, each tuble is a row in the database
conn.commit()
conn.close()

print("[log] found {} reverts !".format(len(reverter_list)))

'''
Get the details of each revert
'''
print("[log] Getting info on each revert ...")

conn = sqlite3.connect(dbName)
cur = conn.cursor()

result_task1 = []

'''
Note: the loop only runs for the first 100 elements, remove the indexing to run it for full database
'''
for index, reverter in enumerate(reverter_list[:100]):
    '''
    Getting Reverted: The person who made the version immediatly following the "earlier version"
    We get this by adding one to the version number for the same title
    '''
    reverter_title, reverter_version = reverter[1], reverter[4]
    query_reverted = "SELECT * from edits WHERE title=? AND version=?"
    cur.execute(query_reverted, (reverter_title, reverter_version + 1))
    reverted = cur.fetchone()

    '''
    Getting the event time:
    This will be the time when the reverter made the change.
    '''
    revert_time = reverter[2]

    '''
    Getting status of reverter: logarithm of number of articles he edited before the revert
    '''
    reverter_id, reverter_user = reverter[0], reverter[5]
    query_status = "SELECT * from edits WHERE (user=? AND time<?)"
    cur.execute(query_status, (reverter_user, revert_time))
    reverted_previous_edits = cur.fetchall()
    reverter_status = log(len(reverted_previous_edits)) if len(reverted_previous_edits) > 0 else 0

    '''
    Getting status of reverted: logarithm of number of articles he edited before the revert
    '''
    if reverted:
        reverted_id, reverted_user = reverted[0], reverted[5]
        query_status = "SELECT * from edits WHERE (user=? AND time<?)"
        cur.execute(query_status, (reverted_user, revert_time))
        reverted_previous_edits = cur.fetchall()
        reverted_status = log(len(reverted_previous_edits)) if len(reverted_previous_edits) > 0 else 0
    else:
        print("[log] No reverted found for the following record: ")
        print(reverter)

    '''
    print the output every 50 reverts
    '''
    if (index % 50) == 0:
        print("------------------------------------------")
        print("[log] Progress: ", index, '\\', len(reverter_list))

    '''
    Add the revert to the list only if it's not a self revert
    '''
    if reverter_user == reverted_user:
        continue

    '''
    Store the revert in the specified format
    '''
    data = {
        'reverter_user': reverter_user,
        'reverted_user': reverted_user,
        'revert_time': revert_time,
        'reverter_status': reverter_status,
        'reverted_status': reverted_status
    }
    result_task1.append(data)

'''
close the database connection
'''
conn.commit()
conn.close()

'''
Store the list of dicts to a pickle file
'''
with open('result_task1.pickle', 'wb') as pickle_out:
    pickle.dump(result_task1, pickle_out)
