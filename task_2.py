import pickle
import pandas as pd
from datetime import datetime, timedelta


def read_pickle(pickleFileName):
    '''
    Read task 1 results from pickle file
    '''
    print("[log] Reading data from pickle file ...")
    with open(pickleFileName, 'rb') as fh:
        reverts = pickle.load(fh)
    print("[log] Reading data complete!")
    return reverts


def list_to_df(data_list):
    '''
    Read the list of dictionaries into a pandas dataframe
    '''
    print("[log] Converting list of dictionaries to pd dataframe ...")

    data_df = pd.DataFrame(data_list)
    num_rows = data_df.shape[0]

    print("[log] Converted {} reverts !".format(num_rows))
    return data_df, num_rows


def convert_time_to_datetime(dataFrame):
    '''
    Make the time column as datetime
    '''
    print("[log] Converting revert_time column to datetime format ...")
    dataFrame['revert_time'] = dataFrame['revert_time'].apply(lambda x: datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    print("[log] Conversion Done!")
    return dataFrame


def get_matching_revert(dataFrame, revert):
    '''
    Find a revert within 24 hours with the reverter and reverted swapped
    '''
    revert_time, reverted_name, reverter_name = revert[1], revert[2], revert[4]
    # Step 1: Create the 24 hour window
    revert_time_plus_day = revert_time + timedelta(days=1)
    # Step 2: Use a mask to get a revert within 24 hours with the reverter and reverted swapped
    mask = (dataFrame['revert_time'] > revert_time) & \
        (dataFrame['revert_time'] < revert_time_plus_day) & \
        (dataFrame['reverter_name'] == reverted_name) & \
        (dataFrame['reverted_name'] == reverter_name)
    matching_reverts = dataFrame[mask]
    return matching_reverts


def main():
    '''
    Read task 1 results from pickle file
    '''
    pickleFileName = 'result_task1.pickle'
    reverts = read_pickle(pickleFileName)

    '''
    If no elements are found in the pickle, exit
    '''
    if not reverts:
        print("Nothing found inside the pickle file! Exiting!")
        quit()

    '''
    Read the list of dictionaries into a pandas dataframe
    '''
    reverts_df, num_reverts = list_to_df(reverts)

    '''
    Make the time column as datetime
    '''
    reverts_df = convert_time_to_datetime(reverts_df)

    '''
    For each revert, try to find a revert within 24 hours with the reverter and reverted swapped
    '''
    result_task2 = []
    progress = 0
    for revert in reverts_df.itertuples():
        '''
        print the output every 100 reverts
        '''
        progress += 1
        if progress % 100 == 0:
            print("[log] Progress:", progress, "\\", num_reverts)

        '''
        Find a revert within 24 hours with the reverter and reverted swapped
        '''
        matching_reverts = get_matching_revert(reverts_df, revert)

        '''
        If no matching reverts were found, move on
        '''
        if matching_reverts.empty:
            continue

        # changing the records to dict format for storage
        matched_dict = matching_reverts.iloc[0].to_dict()
        revert_dict = revert._asdict()

        # Changing the time from TimeSeires format to str
        matched_dict['revert_time'] = str(matched_dict['revert_time'])
        revert_dict['revert_time'] = str(revert_dict['revert_time'])

        '''
        Add the information to the result_task2 list
        '''
        data = {
            'revert_1': matched_dict,
            'revert_2': revert_dict
        }
        result_task2.append(data)

    '''
    Store the list of dicts to a pickle file
    '''
    pickleFileName = 'result_task2.pickle'
    print("[log] Storing {} pairs to {} ...".format(len(result_task2), pickleFileName))
    with open(pickleFileName, 'wb') as pickle_out:
        pickle.dump(result_task2, pickle_out)
    print("[log] Data storage complete! Task 2 is done :D")


if __name__ == "__main__":
    main()
