import pickle
import matplotlib.pyplot as plt

'''
Read task 2 results from pickle file
'''
pickleFileName = 'result_task1.pickle'
print("[log] Reading data from pickle file {} ...".format(pickleFileName))
with open(pickleFileName, 'rb') as fh:
    all_reverts = pickle.load(fh)
print("[log] Reading data complete!")

'''
Read task 2 results from pickle file
'''
pickleFileName = 'result_task2.pickle'
print("[log] Reading data from pickle file {} ...".format(pickleFileName))
with open(pickleFileName, 'rb') as fh:
    abba_reverts = pickle.load(fh)
print("[log] Reading data complete!")

'''
Get a list of status differences
'''
abba_status_diff = [abs(revert['revert_1']['reverter_status'] - revert['revert_1']['reverted_status']) for revert in abba_reverts]
all_status_diff = [abs(revert['reverter_status'] - revert['reverted_status']) for revert in all_reverts]

'''
Get average of differences
'''
avg_abba_status_diff = sum(abba_status_diff) / len(abba_status_diff)
avg_all_status_diff = sum(all_status_diff) / len(all_status_diff)

'''
Print the averages
'''
print("[log] Average ABBA Status difference is {}".format(avg_abba_status_diff))
print("[log] Average Total Status difference is {}".format(avg_all_status_diff))

'''
Plot the two histograms on top of each other
'''
plt.hist(abba_status_diff, bins=50, alpha=0.5, color='b', label='ABBA Status differences')
plt.hist(all_status_diff, bins=50, alpha=0.5, label='All Status differences')
plt.legend(loc='upper right')
plt.show()
